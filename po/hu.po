# Hungarian translation for libshumate.
# Copyright (C) 2022 Free Software Foundation, Inc.
# This file is distributed under the same license as the libshumate package.
#
# Balázs Úr <ur.balazs at fsf dot hu>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: libshumate main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libshumate/issues\n"
"POT-Creation-Date: 2022-08-27 20:44+0000\n"
"PO-Revision-Date: 2022-08-30 01:58+0200\n"
"Last-Translator: Balázs Úr <ur.balazs at fsf dot hu>\n"
"Language-Team: Hungarian <gnome-hu-list at gnome dot org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.12.3\n"

#. m is the unit for meters
#: shumate/shumate-scale.c:197
#, c-format
msgid "%d m"
msgstr "%d m"

#. km is the unit for kilometers
#: shumate/shumate-scale.c:200
#, c-format
msgid "%d km"
msgstr "%d km"

#. ft is the unit for feet
#: shumate/shumate-scale.c:206
#, c-format
msgid "%d ft"
msgstr "%d ft"

#. mi is the unit for miles
#: shumate/shumate-scale.c:209
#, c-format
msgid "%d mi"
msgstr "%d mi"
